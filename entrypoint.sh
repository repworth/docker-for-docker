#!/bin/sh

docker \
 login \
 -u $DOCKER_USERNAME \
 -p $DOCKER_PASSWORD \
 -e $DOCKER_EMAIL \
 $DOCKER_REGISTRY \
& wait

exec "$@"