Docker container with: 
- Docker

## Scripts
login to docker hub with provided credentials and open shell:
```
docker \
  run \
  -it \
  —privileged \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e DOCKER_USERNAME \
  -e DOCKER_PASSWORD \ 
  -e DOCKER_EMAIL \
  -e DOCKER_REGISTRY \
  repworth/docker:docker1.9
```
