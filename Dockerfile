FROM docker:1.9.1

ENV DOCKER_USERNAME=

ENV DOCKER_PASSWORD=

ENV DOCKER_EMAIL=

ENV DOCKER_REGISTRY=

COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/bin/sh"]